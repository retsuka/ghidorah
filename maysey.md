# Maysey

### Combo completion

  so much was missed, 5h into slidehead where you didn't do slidehead, 6h where
  you didn't hfb and this all leads to you having to spend all your time doing
  neutral because your oki can't get used as much


### break pressure

  Potemkin has some of the silliest pressure in the game. You complained that 2p
  6k has a 15f gap. People can't react that quick, if its the only thing you are
  doing they will get out but other than that its a guess. Its hurtbox is
  utterly huge as well so a lot of jump outs cost your opponent meter. Mixing

### genuine oki

  + nobody should be able to press normals on wakeup and be in a situation where
    they are able to win against you
  + safejumps are hugely important for potemkin, they put people in a situation
    where they absolutely must block or they are going to get punished/hit for
    not. Then having blocked then they are in a complete guessing situation
    about your next move and that's where you need to focus your attention on
    habits.

    Focus on getting into these situations, by getting the right knockdowns.
    This will come in time *if* you make it your main focus.


### the right buttons

  + 2p is your go-to round start vs may. She has pretty much nothing that beats
    it and all her things to kill 2p are either ridiculously hard to time or too
    slow.
  + learn to option select your air grabs, j.h is really fudging slow
  + 6h slidehead makes little sense because if it hits they are rolling and you
    lose oki, 6h should really always be followed up by hfb (or hf if you
    confirmed the hit) because you always have the threat of buster
  + your 6p timing was way out, practice confirming whether its going to hit at
    all and yrc'ing it where you think it won't - you will no doubt still be
    able to jump into them, ib and then throw
  + I think you spent a long time just fishing for heavenly and that meant you
    lost focus on neutral
  + backdashing is a really good way to 'anti-air' with potemkin, in fact
    backdashing in neutral is far stronger than it is on wakeup, make them
    commit to something, avoid it and take 1/3 of their life with buster...


### reacting to your opponent's habits

  + quora didn't follow in hoops basically at all in neutral, you needed to
    either react to the summon and 5h him or react to the high hoops and flick
    them (the high ones are really easy to flick, he shouldn't use them as a
    threat as they are really there to stop people jumping...)
  + when you did jump quora was happy to use super to anti-air you because you
    didn't fd against a standing may on your way in
  + quora didn't punish hammerfall on block, you could've gotten away with
    murder ;)


You had so much meter so much of the time that you could've used half of it to
just advance from far out and still have a meter lead. I'm going to ignore the
later bits because you stopped thinking so much - its when you didn't recognise
that quora was just looking for ball to get close to you so he could unblockable
you and didn't adjust to that.



## Positives

- your instant block timing was really good, keep working on it and it will get
  scary
- your defence is pretty strong, just some bad decisions for the most part
- your situational awareness is getting better and better


## Things to do

- utterly grind hammerfall break, by mixing up your strings and using it to get
  close and make people brain fart their way into getting hit by something into
  some of the scariest mixup in gg is a turning point in many games.
  Practice doing all sorts of different strings into it after each other, give
  yourself the challenge of never using the same string twice in a row
- figure out knockdowns and get that combo completion rate up
- mental fortitude
- more often than not, after a blocked 5d you just jump forward h, needs to go
  on your habits list
- test your understanding of the ranges of your buttons, 2s is good but I don't
  think you know how to use it yet
- be patient, getting good with potemkin takes ages and ages and ages
- get the meaty slidehead timing down for the situations where it gives you the
  chance to close ground (I think off of heat knuckle where its not near the
  corner)
