# Dove

- **variety** your ground strings were quite samey, and you committed to them
  really early into the string (I was watching your hands). Slow down the
  buttons for them, its not quite as 'dial-a-combo' as you think it is.
- **mash like crazy** 4ppppppp is ridiculously good. Use it to hit confirm into
  other strings to get the knockdown
- **oki** you have to really earn the big swingy swords, the basic 5s and 5h sword
  sets should be your go-to. Especially midscreen. From a knockdown you get
  _one_ mixup so don't push it and try to get anything really huge. Watch the
  best JP players' setups and recognise when they are settling for weaker setups
  because that's the best you can do there without giving your opponent a week
  to get out. Practice the oki more too, it was a little bit loose. Ram also has
  a bunch of reversal safe sword set stuff so you don't need to be close enough
  for volcanic viper to hit you on Sol's wakeup.
- **wakeup** learn to 6(ph) against late air dashes/empty jump lows and track your
  opponent's habits to see if its something you should use in that match
- **meter** ram only builds meter with the swords, none of her strings actually
  build any so you need to focus on working sword sets, sword buttons etc, into
  your strings. Without them you leave yourself at a huge tension deficit and
  neutral becomes stupidly hard. You also give yourself fewer opportunities to
  get spinny sword oki or even spitty laser yrc mixups. Watch hohiko, kakkou and
  saryu, they are great at working in ways to build meter.
- **burst habits** you burst after the first hit you can, every time you have
  burst - predictable and unsafe bursts hurt ram a hell of a lot because she has
  0 health. Better burst habits please (learn burst safe combo points etc)


## Links

Watch pretty much all of [these](https://keeponrock.in/?char1=ram), hohiko,
saryu and kakkou are ones to watch.

Is 4k ender, jump yrc swords oki still a thing?


She's really hard, the learning curve is really steep but you're doing ok so
just keep looking to beat the people that are better than you are right now.
Getting that really awkward movement, alien tension building etc down will take
some time so just don't give up and use training mode really well (yes, I know
its really bloody boring but you should) and you'll definitely get there. You're
doing ok so far (just stop playing around with other characters)!
