# StrongZero

For you this time just some drills because you just need to take the time to get
the stuff I've already told you down.


## Drill 1

Play without using special moves. End all ground combos at 2d. Air hits I guess you should be allowed to vv or br but that is the *only* time. Any other occasion and you forfeit a round.

**why**: ending strings with things that aren't + or are straight up punishable will get you killed. Use of specials in neutral is a layer on top of what you build using this drill


## Drill 2

Do not jump. Under any circumstances. Do so and you must forfeit a round.

**why**: you will learn the exact use cases for your buttons and get really strong at ground neutral


## Drill 3

No wakeup anything. Force yourself to block and work towards understanding what other characters' mixups look like and how to block them.

**why**: character knowledge and getting better at blocking


Run these drills intensively for many sets against many people. Tell them what
you are doing. Also get into training mode and practice all the things.
